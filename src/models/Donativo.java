package models;

import java.io.Serializable;

public class Donativo implements Serializable {

	private float cantidad;
	private static final long serialVersionUID = -6419920287570445941L;

	public Donativo() {
		this(0f);
	}

	public Donativo(float cantidad) {
		this.cantidad = cantidad;
	}

	public float getCantidad() {
		return cantidad;
	}

	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "Donativo [cantidad=" + cantidad + "]";
	}
	
}
