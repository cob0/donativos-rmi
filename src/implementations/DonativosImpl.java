package implementations;

import java.io.Serializable;
import java.rmi.RemoteException;

import interfaces.DonativosInterface;
import models.Donativo;

public class DonativosImpl implements DonativosInterface, Serializable {

	private float donativos;
	private static final long serialVersionUID = 8045042790764108230L;
	
	public DonativosImpl() {
		this.donativos = 0;
	}

	@Override
	public int addDonativo(Donativo donativo) throws RemoteException {
		int result = 0;
		
		if(donativo != null
				&& donativo.getCantidad() > 0) {
			donativos += donativo.getCantidad();
			result = 1;
		}
		
		return result;
	}

	@Override
	public Donativo checkDonado() throws RemoteException {
		return new Donativo(donativos);
	}

}
