package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import models.Donativo;

public interface DonativosInterface extends Remote {

	int addDonativo(Donativo donativo) throws RemoteException;
	Donativo checkDonado() throws RemoteException;
	
}
