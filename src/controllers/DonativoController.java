package controllers;

import java.util.InputMismatchException;
import java.util.Scanner;

import models.Donativo;

public class DonativoController {

	private Donativo donativo;
	
	public DonativoController(Donativo donativo) {
		this.donativo = donativo;
	}

	public Donativo getDonativo() {
		return donativo;
	}

	public void setDonativo(Donativo donativo) {
		this.donativo = donativo;
	}
	
	public void readCantidad() {
		@SuppressWarnings("resource")
		Scanner teclado = new Scanner(System.in);
		
		do {
			System.out.print("Cantidad a donar: ");
			try {
				donativo.setCantidad(teclado.nextFloat());
			} catch(InputMismatchException e) {
				System.err.println("Introduce solo n�meros.");
			}
		} while(donativo.getCantidad() <= 0);
	}
	
}
