package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import controllers.DonativoController;
import interfaces.DonativosInterface;
import models.Donativo;

public class Client {
	
	public static void main(String[] args) {
		String nombreServicio;		
		Registry registry;
		DonativosInterface donativos;
		DonativoController donativoController = null;
		
		if(System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());
		

		try {
			registry = LocateRegistry.getRegistry("localhost");
			System.out.println("Registro obtenido.");
			
			nombreServicio = "Donativos";
						
			donativos = (DonativosInterface) registry.lookup(nombreServicio);
			donativoController = new DonativoController(new Donativo());
			donativoController.readCantidad();
			if(donativos.addDonativo(donativoController.getDonativo()) > 0);
				System.out.println("Total donativos: " + donativos.checkDonado().getCantidad() + "�");
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
		System.out.println("Saliendo del programa...");
	}

}
